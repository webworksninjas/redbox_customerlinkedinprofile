<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * Package : Redbox
 * Module  : CustomerLinkedinProfile
 * File    : Redbox\CustomerLinkedinProfile\registration.php
 * Date    : 21-05-2018
 * Copyright : Copyright (c) 2018
 * @Author  : Dev-1
 * @Company : Redbox
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Redbox_CustomerLinkedinProfile',
    __DIR__
);

<?php
/**
 *  Copyright © Redbox, Inc. All rights reserved.
 *
 * Package : Redbox
 * Module  : CustomerLinkedinProfile
 * File    : Redbox/CustomerLinkedinProfile/Model/Config/ValidationOptions.php
 * Date    : 24-05-2018
 * Copyright : Copyright (c) 2018
 * @Author  : Dev-1
 * @Company : Redbox
 */

namespace Redbox\CustomerLinkedinProfile\Model\Config;
use Magento\Framework\Option\ArrayInterface;


class ValidationOptions implements ArrayInterface
{
    /**
     * Returns array of select options
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 0, 'label' => __('Required')],
            ['value' => 1, 'label' => __('Optional')],
            ['value' => 2, 'label' => __('Invisible')]
        ];
    }
}
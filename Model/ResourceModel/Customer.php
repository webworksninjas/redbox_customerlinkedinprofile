<?php
/**
 *  Copyright © Redbox, Inc. All rights reserved.
 *
 * Package : Redbox
 * Module  : CustomerLinkedinProfile
 * File    : Redbox/CustomerLinkedinProfile/Model/ResourceModel/Customer.php
 * Date    : 24-05-2018
 * Copyright : Copyright (c) 2018
 * @Author  : Dev-1
 * @Company : Redbox
 */

namespace Redbox\CustomerLinkedinProfile\Model\ResourceModel;

use Magento\Eav\Model\Entity\Context;
use Magento\Framework\Model\ResourceModel\Db\VersionControl\Snapshot;
use Magento\Framework\Model\ResourceModel\Db\VersionControl\RelationComposite;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Validator\Factory;
use Magento\Framework\Stdlib\DateTime;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\InputException;
use Magento\Eav\Model\ResourceModel\Entity\Attribute;


class Customer extends \Magento\Customer\Model\ResourceModel\Customer
{
    /**
     * @var \Magento\Framework\Validator\Factory
     */
    protected $_validatorFactory;

    /**
     * Core store config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\Stdlib\DateTime
     */
    protected $_dateTime;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var $_eavAttribute
     */
    protected $_eavAttribute;
    /**
     * __construct
     * @param Context $context ,
     * @param Snapshot $entitySnapshot ,
     * @param RelationComposite $entityRelationComposite ,
     * @param ScopeConfigInterface $scopeConfig ,
     * @param Factory $validatorFactory ,
     * @param DateTime $dateTime ,
     * @param StoreManagerInterface $storeManager ,
     * @param Session $customerSession ,
     * @param Attribute $eavAttribute,
     * @param $data = []
     */
    public function __construct(
        Context $context,
        Snapshot $entitySnapshot,
        RelationComposite $entityRelationComposite,
        ScopeConfigInterface $scopeConfig,
        Factory $validatorFactory,
        DateTime $dateTime,
        StoreManagerInterface $storeManager,
        Session $customerSession,
        Attribute $eavAttribute,
        array $data = []
    )
    {
        parent::__construct(
            $context, $entitySnapshot,
            $entityRelationComposite, $scopeConfig,
            $validatorFactory, $dateTime, $storeManager, $data
        );
        $this->_customerSession = $customerSession;
        $this->_eavAttribute = $eavAttribute;
    }

    /**
     * Check before saving customer if linked_profile exists in customer
     * means request from customer signup else from checkout
     * @param DataObject $customer
     * @return void
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function _beforeSave(DataObject $customer)
    {
        /** @var \Magento\Customer\Model\Customer $customer */
        if ($customer->getLinkedinProfile()) {
            if (!$this->validateUnique($customer)) {
                throw new InputException(
                    __('This linkedin Url is already taken.')
                );
            }

        } else if (!empty($this->_customerSession->getLinkedInUrl())) {
            
            $linkedInUrl = $this->_customerSession->getLinkedInUrl();
            $customer->setLinkedinProfile($linkedInUrl);
            $this->_customerSession->unsLinkedInUrl();
        }
        parent::_beforeSave($customer);
    }
    /**
     * check uniqueness by comparing to other customer linkedin_profiles
     * @param $customer
     * @return bool
     */
    public function validateUnique($customer)
    {
        $connection = $this->getConnection();
        $bind = ['value' => $customer->getLinkedinProfile()];
        $attributeId = $this->_eavAttribute->getIdByCode(
            'customer', 'linkedin_profile'
        );
        
        $tableName = $connection->getTableName('customer_entity_varchar');
        $select = $connection->select()->from(
            $tableName
        )->where(
            'value = :value'
        );
        
        $bind['attribute_id'] = $attributeId;
        
        $select->where('attribute_id = :attribute_id');
        if ($customer->getId()) {
            $bind['entity_id'] = (int)$customer->getId();
            $select->where('entity_id != :entity_id');
        }
        
        $result = $connection->fetchOne($select, $bind);
        if ($result) {
            return false;
        } else {
            return true;
        }

    }

}

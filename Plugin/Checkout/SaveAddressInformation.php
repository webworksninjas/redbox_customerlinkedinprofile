<?php
/**
 *  Copyright © Redbox, Inc. All rights reserved.
 *
 * Package : Redbox
 * Module  : CustomerLinkedinProfile
 * File    : Redbox/CustomerLinkedinProfile/Plugin/
 *           Checkout/SaveAddressInformation.php
 * Date    : 28-05-2018
 * Copyright : Copyright (c) 2018
 * @Author  : Dev-1
 * @Company : Redbox
 */

namespace Redbox\CustomerLinkedinProfile\Plugin\Checkout;

use Magento\Framework\Exception\InputException;
use Magento\Quote\Model\QuoteRepository;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Customer\Model\ResourceModel\Customer\Collection;
use Magento\Framework\Message\ManagerInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Checkout\Model\ShippingInformationManagement;
use Magento\Checkout\Api\Data\ShippingInformationInterface;


class SaveAddressInformation
{
    /**
     * @var $_quoteRepository
     */
    protected $_quoteRepository;
    /**
     * @var $_customerSession
     */
    protected $_customerSession;
    /**
     * @var $_customerCollection
     */
    protected $_customerCollection;
    /**
     * @var $_messageManager
     */
    protected $_messageManager;
    /**
     * @var $_checkoutSession
     */
    protected $_checkoutSession;

    /**
     * Constructor
     *
     * @param QuoteRepository $quoteRepository ,
     * @param CustomerSession $customerSession ,
     * @param Collection $customerCollection ,
     * @param ManagerInterface $messageManager ,
     * @param CheckoutSession $_checkoutSession
     */
    public function __construct(
        QuoteRepository $quoteRepository,
        CustomerSession $customerSession,
        Collection $customerCollection,
        ManagerInterface $messageManager,
        CheckoutSession $_checkoutSession
    )
    {
        $this->_quoteRepository = $quoteRepository;
        $this->_customerSession = $customerSession;
        $this->_customerCollection = $customerCollection;
        $this->_messageManager = $messageManager;
        $this->_checkoutSession = $_checkoutSession;
    }

    /**
     * beforeSaveAddressInformation
     * @param ShippingInformationManagement $subject
     * @param $cartId
     * @param ShippingInformationInterface $addressInformation
     * @throws InputException
     */
    public function beforeSaveAddressInformation(
        ShippingInformationManagement $subject,
        $cartId,
        ShippingInformationInterface $addressInformation
    )
    {
        $shippingAddress = $addressInformation->getShippingAddress();
        $shippingAddressExtensionAttributes =
            $shippingAddress->getExtensionAttributes();
        
        if (!empty(
            $shippingAddressExtensionAttributes->getCustomerLinkedin()
        )) {
            $check = $this->validateUnique(
                $shippingAddressExtensionAttributes->getCustomerLinkedin()
            );
            if ($check) {
                $this->_customerSession->setLinkedInUrl(
                    $shippingAddressExtensionAttributes->getCustomerLinkedin()
                );
            } else {
                throw new InputException(
                    __('This linkedin Url is already taken.')
                );

            }

        }

    }

    /**
     * Validate For Unique linkedin url
     * @param $url
     * @return bool
     */
    public function validateUnique($url)
    {
        try {
            $collection = $this->_customerCollection->addAttributeToSelect('*')
                ->addAttributeToFilter('linkedin_profile', $url)
                ->load();
            if ($collection->getSize() == 0) {
                return true;
            } else {
                $data = $collection->getFirstItem();
                $repEmail = $data->getEmail();
                $savedEmail = $this->_checkoutSession->getCurrentEmail();
                
                if ($repEmail == $savedEmail) {
                    return true;
                } else {
                    return false;
                }

            }
        } catch (\Exception $e) {
            $this->_messageManager->addError(
                __(
                    'An unspecified error occurred 
                    while getting customer collection.'
                )
            );
        }
    }
}
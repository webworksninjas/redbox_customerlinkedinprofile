<?php
/**
 *  Copyright © Redbox, Inc. All rights reserved.
 *
 * Package : Redbox
 * Module  : CustomerLinkedinProfile
 * File    : Redbox/CustomerLinkedinProfile/Plugin/Checkout
 *           /LinkedInUrlProcessor.php
 * Date    : 25-05-2018
 * Copyright : Copyright (c) 2018
 * @Author  : Dev-1
 * @Company : Redbox
 */

namespace Redbox\CustomerLinkedinProfile\Plugin\Checkout;

use Redbox\CustomerLinkedinProfile\Helper\Data;
use Magento\Customer\Model\Session;
use Magento\Checkout\Block\Checkout\LayoutProcessor;


class LinkedInUrlProcessor
{
    /**
     * @var Data
     */
    protected $_helper;
    /**
     * @var $_session
     */
    protected $_session;

    /**
     * Constructor
     *
     * @param Data $helper
     * @param Session $session
     */
    public function __construct(
        Data $helper,
        Session $session
    )
    {
        $this->_helper = $helper;
        $this->_session = $session;
    }

    /**
     * Checkout LayoutProcessor after process plugin.
     *
     * @param LayoutProcessor $processor
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(LayoutProcessor $processor, $jsLayout)
    {
        if (($this->getLinkedInStatus() == '0' ||
                $this->getLinkedInStatus() == '1') &&
            (!$this->_session->isLoggedIn())) {
            $customAttributeCode = 'customer_linkedin';
            $customField = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'shippingAddress.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'ui/form/element/input',
                    'tooltip' => [
                        'description' => 'Add your Linkedin Profile URL',
                    ],
                ],
                'dataScope' => 'shippingAddress.custom_attributes' . '.' .
                    $customAttributeCode,
                'label' => 'LinkedIn Profile Url',
                'provider' => 'checkoutProvider',
                'sortOrder' => 100,
                'validation' => [
                    'required-entry' => $this->getValidation(),
                    'validate-url' => true
                ],
                'options' => [],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => true,
            ];
            $jsLayout['components']['checkout']
            ['children']['steps']['children']['shipping-step']['children']
            ['shippingAddress']['children']['shipping-address-fieldset']
            ['children'][$customAttributeCode] = $customField;
        }

        return $jsLayout;
    }

    /**
     * get configuration
     * @return bool
     */
    public function getLinkedInStatus()
    {
        return $this->_helper->getLinkedInStatus();
    }

    /**
     * returns false or true on the basis of configuration
     * @return bool
     */
    public function getValidation()
    {
        $linkedinStatus = $this->getLinkedInStatus();

        if ($linkedinStatus == '1' || $linkedinStatus == '2') {
            return false;
        } else {
            return true;
        }
    }
}
<?php
/**
 *  Copyright © Redbox, Inc. All rights reserved.
 *
 * Package : Redbox
 * Module  : CustomerLinkedinProfile
 * File    : Redbox/CustomerLinkedinProfile/Plugin/ConfigPlugin.php
 * Date    : 26-05-2018
 * Copyright : Copyright (c) 2018
 * @Author  : Dev-1
 * @Company : Redbox
 */

namespace Redbox\CustomerLinkedinProfile\Plugin;

use Redbox\CustomerLinkedinProfile\Helper\Data;
use Magento\Eav\Model\Entity\Attribute;
use Magento\Framework\Message\ManagerInterface;
use Magento\Config\Model\Config;


class ConfigPlugin
{
    /**
     * @var $_helper
     */
    protected $_helper;
    /**
     * @var $_entityAttribute
     */
    protected $_entityAttribute;
    /**
     * @var $_messageManager
     */
    protected $_messageManager;

    /**
     * __construct
     * @param Attribute $entityAttribute
     * @param Data $helper
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        Attribute $entityAttribute,
        Data $helper,
        ManagerInterface $messageManager
    )
    {
        $this->_entityAttribute = $entityAttribute;
        $this->_helper = $helper;
        $this->_messageManager = $messageManager;
    }

    /**
     * Called after configuration is saved
     * @param Config $subject
     * @param $result
     * @return
     */
    public function afterSave(
        Config $subject,
        $result
    )
    {
        $fieldStatus = $this->getLinkedInStatus();
        $attributeCode = 'linkedin_profile';
        $entityType = 'customer';
        $attributeInfo = $this->getAttributeInfo($entityType, $attributeCode);
        /* Set validation
        * Required = 0
        * Optional = 1
        * Invisible = 2
        */
        if ($fieldStatus == '0') {
            $attributeInfo->setIsRequired('1');
        } else {
            $attributeInfo->setIsRequired('0');
        }
        try {
            $attributeInfo->save();
        } catch (\Exception $e) {
            $this->_messageManager->addError(
                __('An unspecified error occurred while saving.')
            );
        }

        return $result;
    }

    /**
     * Gives current selected option from configuration
     * @return bool
     */
    public function getLinkedInStatus()
    {
        return $this->_helper->getLinkedInStatus();
    }

    /**
     * Gives attribute object
     * @param $entityType
     * @param $attributeCode
     * @return \Magento\Eav\Model\Entity\Attribute
     */
    public function getAttributeInfo($entityType, $attributeCode)
    {
        $attributeInfo = "";
        try {
            $attributeInfo = $this->_entityAttribute
                ->loadByCode($entityType, $attributeCode);
        } catch (\Exception $e) {
            $this->_messageManager->addError(
                __('An unspecified error occurred while getting attribute.')
            );
        }

        return $attributeInfo;
    }
}
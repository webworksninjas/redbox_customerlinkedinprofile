<?php
/**
 *  Copyright © Redbox, Inc. All rights reserved.
 *
 * Package : Redbox
 * Module  : CustomerLinkedinProfile
 * File    : Redbox/CustomerLinkedinProfile/Plugin/Customer/Controller/Account/EditPost.php
 * Date    : 27-05-2018
 * Copyright : Copyright (c) 2018
 * @Author  : Dev-1
 * @Company : Redbox
 */

namespace Redbox\CustomerLinkedinProfile\Plugin\Customer\Controller\Account;

use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Exception\LocalizedException;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\ResourceModel\Customer\Collection;
use Magento\Framework\Message\ManagerInterface;
use Magento\Customer\Controller\Account\EditPost as CustomerEditPost;


class EditPost
{

    /**
     * @var $_customerRepository
     */
    protected $_customerRepository;
    /**
     * @var $_formKeyValidator
     */
    protected $_formKeyValidator;
    /**
     * @var $_resultRedirectFactory
     */
    protected $_resultRedirectFactory;
    /**
     * @var $_customerCollection
     */
    protected $_customerCollection;
    /**
     * @var $_session
     */
    protected $_session;
    /**
     * @var $_messageManager
     */
    protected $_messageManager;

    /**
     * __construct
     *
     * @param Validator $formKeyValidator
     * @param Context $context
     * @param Collection $customerCollection
     * @param CustomerRepositoryInterface $customerRepository
     * @param Session $customerSession
     * @param ManagerInterface $messageManager
     */
    public function __construct
    (
        Validator $formKeyValidator, Context $context,
        Collection $customerCollection,
        CustomerRepositoryInterface $customerRepository,
        Session $customerSession,
        ManagerInterface $messageManager
    )
    {
        $this->_formKeyValidator = $formKeyValidator;
        $this->_resultRedirectFactory = $context->getResultRedirectFactory();
        $this->_customerCollection = $customerCollection;
        $this->_customerRepository = $customerRepository;
        $this->_session = $customerSession;
        $this->_messageManager = $messageManager;
    }

    /**
     * aroundExecute
     *
     * @param CustomerEditPost $subject
     * @param callable $proceed
     * @return
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function aroundExecute(CustomerEditPost $subject,
                                  callable $proceed)
    {
        $resultRedirect = $this->_resultRedirectFactory->create();
        $validFormKey = $this->_formKeyValidator->validate(
            $subject->getRequest()
        );
        $currentCustomerDataObject = $this->getCustomerDataObject(
            $this->_session->getCustomerId()
        );
        $flag = true;
        if ($validFormKey && $subject->getRequest()->isPost()) {
            if (!empty($subject->getRequest()->getParam('linkedin_profile'))) {
                if ($currentCustomerDataObject->getCustomAttribute(
                    'linkedin_profile'
                )) {
                    $curLinkedInUrl = $currentCustomerDataObject->
                    getCustomAttribute('linkedin_profile')->getValue();
                    $modLinkedInUrl = $subject->getRequest()->
                    getParam('linkedin_profile');
                    if ($curLinkedInUrl != $modLinkedInUrl) {
                        $flag = $this->validateUnique($modLinkedInUrl);
                    }
                } else {
                    $modLinkedInUrl = $subject->getRequest()->
                    getParam('linkedin_profile');
                    $flag = $this->validateUnique($modLinkedInUrl);
                }


            }

            if ($flag) {
                $result = $proceed();
                return $result;
            }
        }

        return $resultRedirect->setPath('*/*/edit');


    }

    /**
     * Get customer data object
     *
     * @param int $customerId
     * @return \Magento\Customer\Api\Data\CustomerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getCustomerDataObject($customerId)
    {
        return $this->_customerRepository->getById($customerId);
    }

    /**
     * Validate For Unique linkedin url
     * @param $url
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return bool
     */
    public function validateUnique($url)
    {
        try {

            $collection = $this->_customerCollection->addAttributeToSelect('*')
                ->addAttributeToFilter('linkedin_profile', $url)
                ->load();
            if ($collection->getSize() == 0) {
                return true;
            } else {
                throw new LocalizedException(
                    __('This linkedin url is already taken.')
                );

            }
        } catch (\Exception $e) {
            $this->_messageManager->addError(
                __($e->getMessage())
            );
        }

    }

}
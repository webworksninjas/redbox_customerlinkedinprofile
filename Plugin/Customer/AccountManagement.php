<?php
/**
 *  Copyright © Redbox, Inc. All rights reserved.
 *
 * Package : Redbox
 * Module  : CustomerLinkedinProfile
 * File    : Redbox/CustomerLinkedinProfile/
 *           Plugin/Customer/AccountManagement.php
 * Date    : 28-05-2018
 * Copyright : Copyright (c) 2018
 * @Author  : Dev-1
 * @Company : Redbox
 */

namespace Redbox\CustomerLinkedinProfile\Plugin\Customer;
use Magento\Checkout\Model\Session;
use Magento\Customer\Model\AccountManagement as CustomerAccountManagement;


class AccountManagement
{

    /**
     * @var $_checkoutSession
     */
    protected $_checkoutSession;

    /**
     * __construct
     *
     * @param Session $_checkoutSession
     */
    public function __construct(Session $_checkoutSession
    )
    {
        $this->_checkoutSession = $_checkoutSession;
    }

    /**
     * Saves current email into session.
     *
     * @param CustomerAccountManagement $subject
     * @param $customerEmail
     * @param $websiteId
     * @return void
     */
    public function beforeIsEmailAvailable(CustomerAccountManagement $subject,
                                           $customerEmail, $websiteId = null)
    {
        $this->_checkoutSession->setCurrentEmail($customerEmail);
    }
}
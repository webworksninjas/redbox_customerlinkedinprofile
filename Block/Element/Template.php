<?php
/**
 *  Copyright © Redbox, Inc. All rights reserved.
 *
 * Package : Redbox
 * Module  : CustomerLinkedinProfile
 * File    : Redbox/CustomerLinkedinProfile/Block/Element/Template.php
 * Date    : 23-05-2018
 * Copyright : Copyright (c) 2018
 * @Author  : Dev-1
 * @Company : Redbox
 */

namespace Redbox\CustomerLinkedinProfile\Block\Element;

use Redbox\CustomerLinkedinProfile\Helper\Data;
use Magento\Framework\View\Element\Template\Context;
use Magento\Customer\Model\Session;
use Magento\Customer\Api\CustomerRepositoryInterface;


class Template extends \Magento\Framework\View\Element\Template
{
    /**
     * @var $_context
     */
    protected $_context;
    /**
     * @var $_customerSession
     */
    protected $_customerSession;
    /**
     * @var $_helper
     */
    protected $_helper;
    /**
     * @var $_customerRepository
     */
    protected $_customerRepository;

    /**
     * __construct
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param $data
     * @param Data $helper
     * @param Session $customerSession
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
        Context $context,
        Data $helper,
        Session $customerSession,
        CustomerRepositoryInterface $customerRepository,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_helper = $helper;
        $this->_customerRepository = $customerRepository;
        $this->_customerSession = $customerSession;


    }

    /**
     * Give true , false or empty  depending upon the configruation
     * @return string
     */
    public function getValidation()
    {
        $configValue = $this->getLinkedInStatus();
        
        if ($configValue == '1') {
            return 'false';
        } else if ($configValue == '2') {
            return '';
        } else {
            return 'true';
        }
    }

    /**
     * Get linkedin configuration
     * @return int
     */
    public function getLinkedInStatus()
    {
        return $this->_helper->getLinkedInStatus();
    }

    /**
     * Get linkedin profile url
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getLinkedInProfile()
    {
        if ($this->getCustomer()->getCustomAttribute('linkedin_profile')) {
            return $this->getCustomer()->
            getCustomAttribute('linkedin_profile')->getValue();
        } else {
            return "";
        }
    }

    /**
     * Get customer by Customer ID.
     * @return \Magento\Customer\Api\Data\CustomerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCustomer()
    {
        $customer = $this->_customerRepository->
            getById($this->_customerSession->getCustomerId());
        
        return $customer;
    }
}
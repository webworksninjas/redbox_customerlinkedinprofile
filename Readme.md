About
======
Magento 2 Module to Add Customer Linkedin Profile URL field for customer registration, account edit and Customer Management section
in magento admin.

Installation
=============

( Using Composer )

Step 1:
If haven't done already, Run below command in CLI :
composer update

Step 2:
Open Magento root compoer.json file


Step 3:
Find "repositories" entry and add module repository path plus update "minimum-stability" and "extra" node in order to set the custom path for the module installation using composer

	"minimum-stability": "dev",
    "repositories": [
        {
            "type": "composer",
            "url": "https://repo.magento.com/"
        },
        {
            "type": "vcs",
            "url": "https://webworksninjas@bitbucket.org/webworksninjas/redbox_customerlinkedinprofile.git"
        }
    ],
    "extra": {
        "magento-force": "override",
        "installer-paths": {
            "app/code/Redbox/CustomerLinkedinProfile": ["redbox/customerlinkedinprofile"]
        }
    }



Step 4 :
Run below command in CLI :
composer require redbox/customerlinkedinprofile

Step 5 :
Run below command in CLI :
php bin/magento setup:upgrade

( Manual Instalation ) :

Step 1 - Download module from git clone https://webworksninjas@bitbucket.org/webworksninjas/redbox_customerlinkedinprofile.git


Step 2 - create "Redbox/CustomerLinkedinProfile" directory in app/code folder and unzip source code within it.


Step 3 - Run php bin/magento setup:upgrade

And you will see module there.



NOTE ( WITHOUT setting up validation proflie file not show up on checkout page ):

After Installation, Login Admin area and go to Store->Settings->Configurations
there you will find tab Redbox click on it and go to Customer Sections, Select validation type and save it.



Requirementes
It require atleast Magento 2.x version and PHP 7.x >

Uninstallation
==============
Step 1 - Run below command in CLI :
php bin/magento module:uninstall Redbox_CustomerLinkedinProfile

Step 2 - Delete folder app/code/redbox

Step 3 - Run php bin/magento setup:upgrade


NOTE : This module is in developer mode so while using composer have to change minimum-stability to dev
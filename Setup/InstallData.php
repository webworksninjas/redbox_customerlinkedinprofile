<?php
/**
 * Package : Redbox
 * Module  : CustomerLinkedinProfile
 * File    : Redbox/CustomerLinkedinProfile/Setup.php
 * Date    : 23-05-2018
 * Copyright : Copyright (c) 2018
 * @Author  : Dev-1
 * @Company : Redbox
 */

namespace Redbox\CustomerLinkedinProfile\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\State;
use Magento\Customer\Model\Customer;


class InstallData implements InstallDataInterface
{

    /**
     * Customer Profile Code
     *
     * @const CUSTOMER_LINKEDIN_ATTR
     */
    const CUSTOMER_LINKED_IN_ATTR = "linkedin_profile";
    /**
     * @var $_messageManager
     */
    protected $_messageManager;
    /**
     * Customer setup factory
     *
     * @var \Magento\Customer\Setup\CustomerSetupFactory
     */
    private $_customerSetupFactory;

    /**
     * Init
     *
     * @param CustomerSetupFactory $customerSetupFactory
     * @param ManagerInterface $messageManager
     * @param State $appState
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        ManagerInterface $messageManager,
        State $appState)
    {
        $appState->setAreaCode('frontend');
        $this->_customerSetupFactory = $customerSetupFactory;
        $this->_messageManager = $messageManager;
    }

    /**
     * Installs DB schema for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function install(ModuleDataSetupInterface $setup,
                            ModuleContextInterface $context)
    {

        $installer = $setup;
        $installer->startSetup();
        $customerSetup = $this->_customerSetupFactory->
        create(['setup' => $setup]);
        $customerSetup->addAttribute(
            Customer::ENTITY,
            self::CUSTOMER_LINKED_IN_ATTR, array(
                "type" => "varchar",
                "backend" => "",
                "label" => "Linkedin Profile",
                "input" => "text",
                "source" => "",
                "visible" => true,
                "required" => true,
                "default" => "",
                "frontend" => "",
                "unique" => true,
                "note" => "",
                'validate_rules' => '{"max_text_length":250}',
                'admin_checkout' => 1)
        );

        $customerAttribute = $customerSetup->getEavConfig()->getAttribute(
            Customer::ENTITY,
            self::CUSTOMER_LINKED_IN_ATTR
        );

        /*
         *  Add field in forms for Customer dashboard, customer create account
         *  and checkout page etc.
        */
        $usedInForms[] = "adminhtml_customer";
        $usedInForms[] = "checkout_register";
        $usedInForms[] = "customer_account_create";
        $usedInForms[] = "customer_account_edit";
        $usedInForms[] = "adminhtml_checkout";

        /*
         *  Save forms references
        */
        $customerAttribute->setData("used_in_forms", $usedInForms)
            ->setData("is_used_for_customer_segment", true)
            ->setData("is_system", 0)
            ->setData("is_user_defined", 1)
            ->setData("is_visible", 1)
            ->setData("sort_order", 100);
        
        try {
            $customerAttribute->save();
        } catch (\Exception $e) {
            $this->_messageManager->addError(
                __(
                    'An unspecified error while saving 
           the custom customer attribute.'
                )
            );
        }

        $installer->endSetup();
    }
}
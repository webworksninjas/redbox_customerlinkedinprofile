<?php
/**
 *  Copyright © RedboxDigital, Inc. All rights reserved.
 *
 * Package : RedboxDigital
 * Module  : CustomerLinkedin
 * File    : Redbox/CustomerLinkedinProfile/Setup/Uninstall.php
 * Date    : 26-05-2018
 * Copyright : Copyright (c) 2018
 * @Author  : Dev-1
 * @Company : RedboxDigital
 */


namespace Redbox\CustomerLinkedinProfile\Setup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\UninstallInterface;
use Magento\Customer\Model\Customer;


class Uninstall implements UninstallInterface
{

    /**
     * @var $_eavSetupFactory
     */
    protected $_eavSetupFactory;
    /**
     * __construct
     *
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->_eavSetupFactory = $eavSetupFactory;
    }
    /**
     * Module uninstall linkedin_profile
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function uninstall(SchemaSetupInterface $setup,
                              ModuleContextInterface $context)
    {
        $setup->startSetup();

        $eavSetup = $this->_eavSetupFactory->create();

        $eavSetup->removeAttribute(Customer::ENTITY, 'linkedin_profile');

        $setup->endSetup();
    }

}